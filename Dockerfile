FROM ubuntu:16.04
MAINTAINER Mahesh Ramananjaiah <maheshkr81@gmail.com>

# Install Kurento Media Server (KMS) 
RUN apt-get update \
    && apt-get install -y wget \
    && echo "deb http://ubuntu.kurento.org xenial kms6" | tee /etc/apt/sources.list.d/kurento.list \
    && wget -O - http://ubuntu.kurento.org/kurento.gpg.key | apt-key add - \
    && apt-get update \
    && apt-get -y dist-upgrade \
    && apt-get -y install openjdk-8-jdk supervisor kurento-media-server-6.0 \ 
    && mkdir -p /var/log/supervisor \
    && rm -rf /var/lib/apt/lists/*

COPY init.sh /opt/kurento/init.sh
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

EXPOSE 8888 9091

CMD ["/usr/bin/supervisord"]
