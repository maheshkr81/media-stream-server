#!/bin/bash

set -e

exec /usr/bin/kurento-media-server "$@"
